package com.scythe.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Base64;

@Configuration
public class GatewayConfig {

    @Value("${openchannel.marketplace.id}")
    private String keyID;

    @Value("${openchannel.marketplace.secret}")
    private String keySecret;

    @Value("${openchannel.marketplace.baseurl}")
    private String baseUrl;

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                        .path("/v2/**")
                        .filters(f -> f.addRequestHeader("authorization", getToken()))
                        .uri(baseUrl))
                .build();
    }

    private String getToken() {
        String encodedUrl = Base64.getUrlEncoder().encodeToString((keyID + ":" + keySecret).getBytes());
        return "Basic " + encodedUrl;
    }
}
