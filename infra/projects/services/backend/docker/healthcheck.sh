#!/bin/bash

url="${HEALTHCHECK_URL}"

getStatus()
{
    echo $2
}

s2xx=$(curl -i -s $url | grep HTTP)

s2=$(getStatus $s2xx)
if [[ "$s2" -ge 200  && "$s2" -lt 300 ]]; then
    exit 0
else
    exit 1
fi
