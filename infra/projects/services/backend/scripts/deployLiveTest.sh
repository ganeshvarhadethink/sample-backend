#!/bin/bash

if [ -z "$SITENAME_TEST" ]; then
    echo "There is no SITENAME_TEST"
else
    export BACKEND_STACKNAME="$CLUSTER_NAME-$SERVICE_NAME-Test"
    export BACKEND_SITENAME="${CONFIG_FILE_SUFFIX}-$SERVICE_NAME-test"
    export TARGET_GROUP_NAME="`./getTargetGroupName.sh`"
    export BACKEND_SITENAME="$SITENAME_TEST"
    export BACKEND_PROJECTNAME="$BACKEND_STACKNAME"
    export LOAD_BALANCER_CNAME_WITH_UNDERLINE="`echo $BACKEND_SITENAME | sed s/[.]/_/g`"
    if [ "$CONFIG_FILE_SUFFIX" == "eu" ]; then
        export CONFIG_FILE_SUFFIX="eu-test"
        ./deployCloudformation.sh
        export CONFIG_FILE_SUFFIX="eu"
    else
        ./deployCloudformation.sh
    fi
    echo "$SITENAME_TEST"
fi
if [ -z "$SITENAME_LIVE" ]; then
    echo "There is no SITENAME_LIVE"
else
    export BACKEND_STACKNAME="$CLUSTER_NAME-$SERVICE_NAME-Live"
    export BACKEND_SITENAME="${CONFIG_FILE_SUFFIX}-$SERVICE_NAME-live"
    livesite="`./getTargetGroupName.sh`"
    export TARGET_GROUP_NAME="${livesite}lv"
    export BACKEND_SITENAME="$SITENAME_LIVE"
    export BACKEND_PROJECTNAME="$BACKEND_STACKNAME"
    export LOAD_BALANCER_CNAME_WITH_UNDERLINE="`echo $BACKEND_SITENAME | sed s/[.]/_/g`"
    if [ "$CONFIG_FILE_SUFFIX" == "eu" ]; then
        export CONFIG_FILE_SUFFIX="eu-live"
        ./deployCloudformation.sh
        export CONFIG_FILE_SUFFIX="eu"
    else
        ./deployCloudformation.sh
    fi
    echo "$SITENAME_LIVE"
fi