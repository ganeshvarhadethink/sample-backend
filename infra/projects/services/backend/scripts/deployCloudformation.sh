#!/bin/bash
set -e
set -x

timestamp="`date +%s`"
TGPRI=${timestamp: -4}

aws cloudformation deploy --region $REGION --stack-name $BACKEND_STACKNAME-elb --capabilities CAPABILITY_NAMED_IAM --no-fail-on-empty-changeset --template-file service-elb.yml --parameter-overrides \
AlbHealthCheckHealthyThreshold='3' \
AlbHealthCheckInterval='15' \
AlbHealthCheckPath='/actuator/health' \
AlbHealthCheckTimeout='5' \
AlbHealthCheckUnhealthyThreshold='5' \
CertificateArn="$CERTIFICATE_CLUSTER" \
ClusterName="$CLUSTER_NAME" \
ContainerNetworkMode='awsvpc' \
ContainerPort='8080' \
HasHTTPS='true' \
SiteName="$BACKEND_SITENAME" \
SiteTgPriority="$TGPRI" \
ProjectName="${BACKEND_PROJECTNAME}" \
TargetGroupName="${TARGET_GROUP_NAME}"

aws cloudformation deploy --region $REGION --stack-name $BACKEND_STACKNAME-ecs --capabilities CAPABILITY_NAMED_IAM --no-fail-on-empty-changeset --template-file service-ecs.yml --parameter-overrides \
ClusterName="${CLUSTER_NAME}" \
ProjectName="${BACKEND_PROJECTNAME}" \
EcsTasksNumber="$REPLICA_DESIRED" \
ContainerPort="8080" \
ContainerVirtualCPUs="$FARGATE_CPU_UNIT" \
ContainerMemory="$FARGATE_MEMORY" \
DockerImageVersion="${VERSION_IMAGE}" \
AlbHealthCheckStartPeriod="200" \
ContainerNetworkMode="awsvpc" \
EcsTasksMax="$REPLICA_MAX" \
EcsTasksMin="$REPLICA_MIN" \
EcsScaleInAdjustment='1' \
EcsScaleOutAdjustment='-1' \
EcsScaleInRate='300' \
EcsScaleOutRate='300' \
ImageRepo="${ECR_REPO_URI}" \
TargetGroupName="${TARGET_GROUP_NAME}" \
SpringProfileActive="${CONFIG_FILE_SUFFIX}" \
UrlHealthCheck="https://${BACKEND_SITENAME}/actuator/health"

aws cloudformation deploy --region $REGION --stack-name $BACKEND_STACKNAME-route53 --capabilities CAPABILITY_NAMED_IAM --no-fail-on-empty-changeset --template-file service-route53.yml --parameter-overrides \
ClusterName="$CLUSTER_NAME" \
LoadBalancerCname="$BACKEND_SITENAME" \
HostedZoneName="$HOSTED_ZONE"

aws cloudformation deploy --region $REGION --stack-name $BACKEND_STACKNAME-alarms --capabilities CAPABILITY_NAMED_IAM --no-fail-on-empty-changeset --template-file service-alarms.yml --parameter-overrides \
ClusterName="$CLUSTER_NAME" \
LoadBalancerCnameWithUnderline="$LOAD_BALANCER_CNAME_WITH_UNDERLINE" \
UnHealthyAlarmRedLimit='2' \
ProjectName="${BACKEND_PROJECTNAME}" \
TargetGroupName="${TARGET_GROUP_NAME}"