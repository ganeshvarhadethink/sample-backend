AWSTemplateFormatVersion: 2010-09-09
Description: Sets up service management-dashboard on fargate
Parameters:
  ClusterName:
    Type: String

  ProjectName:
    Type: String

  ImageRepo:
    Type: String

  DockerImageVersion:
    Type: String

  TargetGroupName:
    Type: String

  ContainerMemory:
    Type: String

  ContainerNetworkMode:
    Type: String

  ContainerPort:
    Type: String

  ContainerVirtualCPUs:
    Type: String

  EcsTasksNumber:
    Type: String

  AlbHealthCheckStartPeriod:
    Type: String

  SpringProfileActive:
    Type: String

  UrlHealthCheck:
    Type: String

Conditions:
  hasOnlyOneTask: !Equals [!Ref EcsTasksNumber, "1"]

Resources:

  ExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub ${ProjectName}_ExecutionRole
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'


  TaskRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub ${ProjectName}_TaskRole
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'

  servicetaskdefinitionWEB:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Sub ${ProjectName}
      NetworkMode: !Sub ${ContainerNetworkMode}
      RequiresCompatibilities:
        - FARGATE
      Cpu: !Sub ${ContainerVirtualCPUs}
      Memory: !Sub ${ContainerMemory}
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        -
          Name: !Sub ${ProjectName}__Fargate_Application
          Image: !Sub ${ImageRepo}:${DockerImageVersion}
          PortMappings:
            - ContainerPort: !Ref ContainerPort
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Sub /ecs-cluster/${ClusterName}
              awslogs-stream-prefix: /ecs-task-output
              awslogs-region: !Ref AWS::Region
          Environment:
            - Name: AWS_ACCESS_KEY_ID
              Value: !Join ['', ['{{resolve:secretsmanager:/ECS-CLUSTER/', !Sub '${ClusterName}', '/$SERVICE_NAME-secrets:SecretString:AWS_ACCESS_KEY_ID}}' ]]
            - Name: AWS_SECRET_ACCESS_KEY
              Value: !Join ['', ['{{resolve:secretsmanager:/ECS-CLUSTER/', !Sub '${ClusterName}', '/$SERVICE_NAME-secrets:SecretString:AWS_SECRET_ACCESS_KEY}}' ]]
            - Name: spring_profiles_active
              Value: !Ref SpringProfileActive
            - Name: HEALTHCHECK_URL
              Value: !Ref UrlHealthCheck

  serviceECSServiceWEB:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: !Sub ${ProjectName}
      Cluster:
        'Fn::ImportValue': !Sub ${ClusterName}ECSCluster
      TaskDefinition: !Ref servicetaskdefinitionWEB
      DeploymentConfiguration:
        MinimumHealthyPercent: !If [hasOnlyOneTask, 0, 50]
        MaximumPercent: 100
      DesiredCount: !Ref EcsTasksNumber
      HealthCheckGracePeriodSeconds: !Ref AlbHealthCheckStartPeriod
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          # change to DISABLED if you're using private subnets that have access to a NAT gateway
          AssignPublicIp: DISABLED
          Subnets:
            - 'Fn::ImportValue': !Sub market-dev1-clusterPrivateSubnet1
            - 'Fn::ImportValue': !Sub market-dev1-clusterPrivateSubnet2
          SecurityGroups:
            - 'Fn::ImportValue': !Sub market-dev1-clusterInternalAccessSecurityGroup
      LoadBalancers:
        -
          ContainerName: !Sub ${ProjectName}__Fargate_Application
          ContainerPort: !Ref ContainerPort
          TargetGroupArn:
            'Fn::ImportValue': !Sub ${TargetGroupName}-tg

